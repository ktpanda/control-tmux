# Set up commands to behave more like 'screen'
set-option -g prefix C-a
unbind-key C-b

# Press Ctrl-A, A to send Ctrl-A to the inner terminal
bind-key a send C-a

# Press Ctrl-A, Ctrl-A to switch to the most recently used window in the current session
# (marked by a "-" on the status bar)
bind-key C-a last-window

# Press Ctrl-A, Ctrl-C or Ctrl-A, C to open a new window in the current ession
bind-key C-c new-window -c ~
bind-key c new-window -c ~

# Press Ctrl-A, R to reload ~/.tmux.conf
bind-key R source-file ~/.tmux.conf \; display-message "done"

# Press Ctrl-A, Space to move to the next window
bind-key Space next-window

# Press Ctrl-A, Left or Ctrl-A, Right to move to the previous/next window
bind-key -r Left previous-window
bind-key -r Right next-window
bind-key -r C-s previous-window
bind-key -r C-x next-window
bind-key n next-window
bind-key p previous-window


# Press Ctrl-A, L to link a window from another session into the current one (e.g. Ctrl-A, L,
# "editor:1", Enter will link the first window in the "editor" session into the current session)
bind-key l command-prompt -p link-window "link-window -s '%%'"

# Press Ctrl-A, K to remove the window from the current session (only if it is linked from another session)
bind-key k unlink-window

set-option -g prefix2 M-=
bind-key = send M-=
unbind-key Up
unbind-key Down

bind-key P paste-buffer

# Press Ctrl-A, # to switch to a specific window in the current session
bind-key 1 select-window -t :1
bind-key 2 select-window -t :2
bind-key 3 select-window -t :3
bind-key 4 select-window -t :4
bind-key 5 select-window -t :5
bind-key 6 select-window -t :6
bind-key 7 select-window -t :7
bind-key 8 select-window -t :8
bind-key 9 select-window -t :9
bind-key 0 select-window -t :10

# Press Ctrl-A, Ctrl-P to begin logging output from the current window
bind-key C-p pipe-pane 'cat >>~/output-#S:#I.#P' \; display-message "pipe to output-#S:#I.#P"

# Press Ctrl-A, Ctrl-O to stop logging
bind-key C-o pipe-pane \; display-message "pipe closed"

#bind-key -t emacs-copy \/ search-forward
#bind-key -t emacs-copy Enter copy-selection

# Press Ctrl-A, Ctrl-G to go into "RAW" mode. In this mode, the status bar is hidden, and the prefix
# for all commands is changed from Ctrl-A to Ctrl-Q. This makes it easier if you are running ssh
# inside tmux, and you want to run tmux on the server you are logged into.
#
# That way, pressing Ctrl-A will affect the remote tmux session instead of the current one. You can
# still press Ctrl-Q to send commands to the current session (for instance, Ctrl-Q, D to detach). If
# you need to send Ctrl-Q to the remote session, you can press Ctrl-Q, Q. To exit raw mode, press
# Ctrl-Q, Ctrl-Q.

unbind-key -n C-q
bind-key C-q set -u set-titles-string \; set -u prefix \; set -u prefix2 \; set status on
bind-key q send C-q
bind-key g set set-titles-string "[RAW #S:#I] #T" \; set prefix C-q \; set status off

# COLORS.
# For reference, the xterm color cube points are: 00, 5F, 87, AF, D7, FF
# Status bar has a dim gray background
set -g status-left "#{?client_prefix,#[bg=#d78700]#[fg=0],}[#S]#[bg=colour234] "

set-option -g status-style bg=colour234,fg=colour0

# Left shows the session name, in blue
set-option -g status-left-style fg=colour74

set-window-option -g allow-rename off
set-window-option -g automatic-rename on

# set-option -g -s quiet on

# Windows are medium gray; current window is white
set-window-option -g window-status-style fg=colour244
set-window-option -g window-status-current-style fg=colour15,bg=colour234
# Beeped windows get a blinding orange background
set-window-option -g window-status-bell-style 'fg=#000000,bg=#d78700'
# set-window-option -g window-status-bell-attr none

set-option -g set-titles on
set-option -g set-titles-string '[tmux #S:#I] #T'
set-option -g status-right ''
set-option -sg escape-time 50
set-option -g default-terminal screen-256color
set-option -g base-index 1
set-window-option -g xterm-keys on
